$(function(){

    var selectSaida = $('#selectSaida')

    var selectEntrada = $('#selectEntrada')

  function moveItems(sourseSelect, destSelect) {
     $(sourseSelect).find(':selected').appendTo(destSelect)

  }

  $('#adiciona').click(function () {
        moveItems(selectSaida, selectEntrada);

  });

  $('#remove').on('click', function () {
      moveItems(selectEntrada, selectSaida);
  });

  $('#filtro').keyup(function(){
    var texto = $(this).val().toUpperCase();
    $(selectSaida).find('option').each(function(){
        var resultado = $(this).text().toUpperCase().indexOf(texto);
        if(resultado < 0) {
            $(this).fadeOut();
        }else {
            $(this).fadeIn();
        }
    });
  });

  $('#salvar').click(function (){
     $(selectEntrada).find('option').prop('selected',true);
  });


});

