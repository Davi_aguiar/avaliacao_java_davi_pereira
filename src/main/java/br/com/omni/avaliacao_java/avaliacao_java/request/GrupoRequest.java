package br.com.omni.avaliacao_java.avaliacao_java.request;

import br.com.omni.avaliacao_java.avaliacao_java.model.DepProduto;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class GrupoRequest {

    private Long id;

    @NotBlank(message = "O Nome do Grupo não pode ser vazio")
    private String nome;

    @NotEmpty(message = "O Grupo deve ter ao menos 1 Produto")
    private List<DepProduto> produto;


}
