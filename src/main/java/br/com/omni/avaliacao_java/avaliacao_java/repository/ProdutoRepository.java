package br.com.omni.avaliacao_java.avaliacao_java.repository;

import br.com.omni.avaliacao_java.avaliacao_java.model.DepProduto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdutoRepository extends JpaRepository<DepProduto, Long> {

}
