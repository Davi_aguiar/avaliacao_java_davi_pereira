package br.com.omni.avaliacao_java.avaliacao_java.usercasa;

import br.com.omni.avaliacao_java.avaliacao_java.model.DepProduto;
import br.com.omni.avaliacao_java.avaliacao_java.model.Grupo;
import br.com.omni.avaliacao_java.avaliacao_java.repository.GrupoRepository;
import br.com.omni.avaliacao_java.avaliacao_java.request.GrupoRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AtualizarGrupo {

    private final GrupoRepository grupoRepository;

    public Grupo executar(GrupoRequest updDTO){
        return updateGrupo(updDTO);
    }

    private Grupo updateGrupo(GrupoRequest updDTO){

        Grupo grupo = grupoRepository.getOne(updDTO.getId());

        grupo.setNome(updDTO.getNome());
        grupo.setProduto(updDTO.getProduto());
        grupoRepository.save(grupo);

        return grupo;

    }


}
