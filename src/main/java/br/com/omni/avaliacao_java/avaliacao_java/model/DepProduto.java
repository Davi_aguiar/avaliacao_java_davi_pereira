package br.com.omni.avaliacao_java.avaliacao_java.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "produtos")
@Entity
public class DepProduto {

    @Id
    @GeneratedValue(generator="produto_id")
    @SequenceGenerator(name="produto_id", sequenceName = "produto_id", allocationSize = 1)
    @Column(name ="id")
    private Long id;

    @Column(name = "nome")
    private String nome;


    @Override
    public String toString() {
        return nome;
    }
}
