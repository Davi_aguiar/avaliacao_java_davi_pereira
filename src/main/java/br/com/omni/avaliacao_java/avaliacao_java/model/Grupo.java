package br.com.omni.avaliacao_java.avaliacao_java.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Table(name = "grupo_produtos")
@Entity
public class Grupo {

    @Id
    @GeneratedValue(generator="grupo_id")
    @SequenceGenerator(name="grupo_id", sequenceName = "grupo_id", allocationSize = 1)
    @Column(name ="id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @ManyToMany
    private List<DepProduto> produto;

}
