package br.com.omni.avaliacao_java.avaliacao_java.usercasa;

import br.com.omni.avaliacao_java.avaliacao_java.model.Grupo;
import br.com.omni.avaliacao_java.avaliacao_java.repository.GrupoRepository;
import br.com.omni.avaliacao_java.avaliacao_java.request.GrupoRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RegistraGrupo {

    @Autowired
    private GrupoRepository grupoRepository;

    public Grupo executar(GrupoRequest authDTO){
        return  criaGrupo(authDTO);
    }


    private Grupo criaGrupo(GrupoRequest authDTO){

        Grupo grupo = new Grupo();

        grupo.setNome(authDTO.getNome());
        grupo.setProduto(authDTO.getProduto());
        grupoRepository.save(grupo);
        return grupo;

    }

}
