package br.com.omni.avaliacao_java.avaliacao_java.controller;

public class URLMapping {

    private  URLMapping(){}

    public static final String ROOT_PUBLIC_PATH = "/api/public";

    public static final String ROOT_API_GRUPO = ROOT_PUBLIC_PATH + "/grupo";

}
