package br.com.omni.avaliacao_java.avaliacao_java.controller;

import br.com.omni.avaliacao_java.avaliacao_java.model.DepProduto;
import br.com.omni.avaliacao_java.avaliacao_java.model.Grupo;
import br.com.omni.avaliacao_java.avaliacao_java.repository.GrupoRepository;
import br.com.omni.avaliacao_java.avaliacao_java.repository.ProdutoRepository;
import br.com.omni.avaliacao_java.avaliacao_java.request.GrupoRequest;
import br.com.omni.avaliacao_java.avaliacao_java.usercasa.AtualizarGrupo;
import br.com.omni.avaliacao_java.avaliacao_java.usercasa.RegistraGrupo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static br.com.omni.avaliacao_java.avaliacao_java.controller.URLMapping.ROOT_API_GRUPO;


@Controller
@RequestMapping(path = ROOT_API_GRUPO)
public class GruposController {

    @Autowired
    private GrupoRepository grupoRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    RegistraGrupo registraGrupo;

    @Autowired
    AtualizarGrupo atualizarGrupo;

    //listar os grupos na pagina home
    @GetMapping("list")
    public String listar(Model model,
                         @PageableDefault(sort = "id", direction = Sort.Direction.ASC, page = 0, size = 5) Pageable paginacao,
                         @RequestParam("page") Optional<Integer> page,
                         @RequestParam("size") Optional<Integer> size){

        Page<Grupo> grupos = grupoRepository.findAll(paginacao);
        model.addAttribute("grupos", grupos);

        int totalPages = grupos.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "home";
    }

    //get para o formulario com os valores de produtos
    @GetMapping("form")
    public String getForm(GrupoRequest authDTO, Model model){
        List<DepProduto> produtos = produtoRepository.findAll();
        model.addAttribute("produtos", produtos);
        return "group/form";

    }

    //post para salvar dados cadastrado do grupo
    @PostMapping("new")
    public String registraGrupo(@Valid GrupoRequest authDTO, BindingResult result, RedirectAttributes redirectAttributes,Model model){
        if (result.hasErrors()){
            return getForm(authDTO, model);
        }
        else {
            Grupo grupo = registraGrupo.executar(authDTO);
            redirectAttributes.addFlashAttribute("msgSucesso", "O Grupo " + authDTO.getNome() + " foi criado com sucesso");
            return "redirect:" + ROOT_API_GRUPO + "/list";
        }

    }

    //get para requisita a pagina de edicao
    @GetMapping("getupdate/{id}")
    public String getFormUpdt(@PathVariable("id") Long id, Model model, GrupoRequest updDTO, RedirectAttributes redirectAttributes){
        Optional<Grupo> getGrupoId = grupoRepository.findById(updDTO.getId());

        if (getGrupoId.isPresent()){
            List<DepProduto> produtosUsuado = new ArrayList<>(getGrupoId.get().getProduto());
            List<DepProduto> produtosDisponiveis = new ArrayList<>();
            List<DepProduto> produtos = produtoRepository.findAll();
            produtos.forEach(p1 ->{
                AtomicBoolean igual = new AtomicBoolean(false);
                produtosUsuado.forEach(p2 ->{
                    if (p1.getId().equals(p2.getId()))
                        igual.set(true);
                });
                if (!igual.get())
                    produtosDisponiveis.add(p1);
            });
            model.addAttribute("grupos", getGrupoId.get() );
            model.addAttribute("produtosUsados", produtosUsuado );
            model.addAttribute("produtos", produtosDisponiveis);
            return "group/update";
        }
        else {
            redirectAttributes.addFlashAttribute("message", "O Grupo buscado Não existe");
            return "redirect:" + ROOT_API_GRUPO + "/list";

        }

    }

    //Post para a response da pagina de edicao
    @PostMapping("update")
    public String update(@Valid GrupoRequest updDTO, BindingResult result, RedirectAttributes redirectAttributes, Model model){
        if (result.hasErrors()){
            return getFormUpdt(updDTO.getId(), model, updDTO, redirectAttributes);
        }
        Grupo grupoupdt = atualizarGrupo.executar(updDTO);
        redirectAttributes.addFlashAttribute("msgSucesso", "O Grupo " + updDTO.getNome() + " Foi modificado com sucesso");
         return "redirect:" + ROOT_API_GRUPO + "/list";
    }

    //get para detalhar
    @GetMapping("details/{id}")
    public String getDetails(@PathVariable("id") Long id, Model model){
        Optional<Grupo> getGrupoId = grupoRepository.findById(id);

        if (getGrupoId.isPresent()){
            List<DepProduto> produtosUsados = new ArrayList<>(getGrupoId.get().getProduto());
            List<DepProduto> produtosDisponiveis = new ArrayList<>();
            List<DepProduto> produtos = produtoRepository.findAll();
            produtos.forEach(p1 ->{
                AtomicBoolean igual = new AtomicBoolean(false);
                produtosUsados.forEach(p2 ->{
                    if (p1.getId().equals(p2.getId()))
                        igual.set(true);
                });
                if (!igual.get())
                    produtosDisponiveis.add(p1);
            });
            model.addAttribute("grupos", getGrupoId.get());
            model.addAttribute("produtosUsados", produtosUsados);
            model.addAttribute("produtos", produtosDisponiveis);
            return "group/details";
        }
        else {
            return "redirect:" + ROOT_API_GRUPO + "/list";

        }
    }

    //função de delete grupo
    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){
        grupoRepository.deleteById(id);
        redirectAttributes.addFlashAttribute("msgExcluir", "Registro removido com sucesso");
        return "redirect:" + ROOT_API_GRUPO + "/list";
    }



}
